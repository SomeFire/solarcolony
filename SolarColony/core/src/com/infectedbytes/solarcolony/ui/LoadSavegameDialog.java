package com.infectedbytes.solarcolony.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.utils.Array;
import com.infectedbytes.solarcolony.GameState;
import com.infectedbytes.solarcolony.Main;
import com.infectedbytes.solarcolony.Resources;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Henrik on 15.01.2016.
 */
public class LoadSavegameDialog extends Dialog {
    private final Main main;
    private final FileHandle savegames = Gdx.files.local("savegames");
    private final List<Entry> listbox;

    public LoadSavegameDialog(Main main) {
        super("Load Savegame", Resources.skin);
        this.main = main;
        getContentTable().add(new Label("Select a savegame to load: ", Resources.skin)).row();

        ScrollPane pane = new ScrollPane(listbox = new List<Entry>(Resources.skin));
        listbox.setItems(getEntries());
        getContentTable().add(pane).maxHeight(250);

        button("Load", this);
        button("Cancel");
    }

    @Override
    protected void result(Object object) {
        if(object != null && listbox.getSelected() != null) {
            main.push(new GameState(main, listbox.getSelected().savegame));
        }
    }

    @Override
    public void hide() {
        removeCaptureListener(ignoreTouchDown);
        remove();
    }

    private Array<Entry> getEntries() {
        Array<Entry> entries = new Array<Entry>();
        for(FileHandle handle : savegames.list()) entries.add(new Entry(handle));
        return entries;
    }

    private class Entry {
        private String savegame;
        private String text;

        public Entry(FileHandle handle) {
            savegame = handle.name();
            text = savegame + " (" + DateFormat.getDateInstance().format(new Date(handle.lastModified())) + ")";
        }

        @Override
        public String toString() { return text; }
    }
}
