package com.infectedbytes.solarcolony;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.infectedbytes.solarcolony.ui.LoadSavegameDialog;
import com.infectedbytes.solarcolony.ui.SavegameDialog;

/**
 * Created by Henrik on 14.01.2016.
 */
public class StartScreen implements ScreenState {
    private Main main;
    private Stage stage;
    private float totalTime;
    private Table table;

    public StartScreen(final Main main) {
        final int width = 100;
        this.main = main;
        this.stage = new Stage(new ScreenViewport());
        TooltipManager.getInstance().initialTime = 0;
        table = new Table();
        table.setFillParent(true);
        table.add(new Label("Solar Colony", Resources.skin, "big")).row();
        table.add(new Label("Build your own space station!", Resources.skin)).row();
        table.add(new Label("Choose a difficulty for a new game:", Resources.skin)).row();
        {
            final Table level = new Table();
            level.add(levelButton("Easy", "Low consume of humans.\n" +
                    "Restore 100% of the resources when destroying a building.", "level/easy.sav")).width(width);
            level.add(levelButton("Medium", "Medium consume of humans.\n" +
                    "Restore 75% of the resources when destroying a building.", "level/medium.sav")).width(width);
            level.add(levelButton("Hard", "High consume of humans.\n" +
                    "Restore 50% of the resources when destroying a building.", "level/hard.sav")).width(width);
            table.add(level).row();
        }
        table.add(new Label("Or load savegame:", Resources.skin)).row();
        final TextButton load = new TextButton("Load", Resources.skin);
        load.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) { new LoadSavegameDialog(main).show(stage); }
        });
        table.add(load).width(width).row();
        table.add(new Label("\n\n", Resources.skin)).row();
        final TextButton exit = new TextButton("Exit", Resources.skin);
        exit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) { Gdx.app.exit(); }
        });
        table.add(exit).width(width);

        stage.addActor(table);
    }

    private TextButton levelButton(String text, String tooltip, final String file) {
        TextButton btn = new TextButton(text, Resources.skin);
        btn.addListener(new TextTooltip(tooltip, Resources.skin));
        btn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) { new SavegameDialog(main, file).show(stage); }
        });
        return btn;
    }

    @Override
    public void update(float delta) {
        totalTime += delta;
        stage.act(delta);
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(stage.getCamera().combined);
        batch.begin();
        batch.draw(Resources.background, (float)(Math.sin(totalTime / 50) * 200 - 200), (float)(Math.cos(totalTime / 50) * 100 - 100));
        batch.end();
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public InputProcessor inputProcessor() {
        return stage;
    }
}
