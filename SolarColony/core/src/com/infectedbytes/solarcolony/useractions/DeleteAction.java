package com.infectedbytes.solarcolony.useractions;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.infectedbytes.solarcolony.*;

/**
 * Created by Henrik on 14.01.2016.
 */
public class DeleteAction extends UserAction {
    private int x, y;
    private boolean leftDown;
    private Room room;

    public DeleteAction(GameState state) {
        super(state);
    }

    @Override
    public void render(SpriteBatch batch) {
        if(room != null) {
            batch.end();
            float rx = room.position.x;
            float ry = room.position.y;
            float sx = room.size.x;
            float sy = room.size.y;
            gameState.shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
            gameState.shapeRenderer.setColor(Color.RED);
            gameState.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            gameState.shapeRenderer.rect(rx, ry, sx, sy);
            gameState.shapeRenderer.line(rx, ry, rx + sx, ry + sy);
            gameState.shapeRenderer.line(rx, ry + sy, rx + sx, ry);
            gameState.shapeRenderer.end();
            batch.begin();
        }
        batch.draw(Resources.delete, x, y);
    }

    @Override
    public void mouseDown(int worldX, int worldY, int button) {
        super.mouseDown(worldX, worldY, button);
        if(button == 0) {
            leftDown = true;
            handle(worldX, worldY);
        }
    }

    @Override
    public void mouseUp(int worldX, int worldY, int button) {
        super.mouseUp(worldX, worldY, button);
        if(button == 0) leftDown = false;
    }

    @Override
    public void mouseDragged(int worldX, int worldY) {
        super.mouseDragged(worldX, worldY);
        if(leftDown) handle(worldX, worldY);
    }

    @Override
    public void mouseMoved(int worldX, int worldY) {
        updatePos(worldX, worldY);
    }

    private void updatePos(int worldX, int worldY) {
        x = Util.floorCoord(worldX) + 8;
        y = Util.floorCoord(worldY) + 8;
        room = gameState.getWorld().getRoom(x, y);
    }

    private void handle(int worldX, int worldY) {
        updatePos(worldX, worldY);
        if(!gameState.getWorld().canDestroy(room)) return;
        gameState.getWorld().destroy(room);
        //try move resources
        for(Storage storage : room.storages) {
            for(Quantity q : storage) {
                gameState.getWorld().addGlobalResource(q);
                //if(q.amount > 0) System.out.println("Discard " + q.amount + " " + q.resource.shortName);
            }
        }
        // restore some costs
        for(Quantity q : room.buildCost) {
            q.amount = (int)Math.ceil(q.amount * gameState.getWorld().demolishFactor);
            gameState.getWorld().addGlobalResource(q);
        }
        room = null;
    }
}
