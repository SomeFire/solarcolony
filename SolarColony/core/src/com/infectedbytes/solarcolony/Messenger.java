package com.infectedbytes.solarcolony;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.infectedbytes.solarcolony.enums.Message;

/**
 * Created by Henrik on 09.01.2016.
 */
public final class Messenger {
    public static MessageDispatcher manager = MessageManager.getInstance();
    private Messenger() {}

    public static void dispatch(Message msg, Object data) {
        manager.dispatchMessage(msg.ordinal(), data);
    }

    public static void dispatch(Message msg, Telegraph sender, Telegraph receiver, Object data) {
        manager.dispatchMessage(sender, receiver, msg.ordinal(), data);
    }

    public static void addListener(Telegraph telegraph, Message... ids) {
        for(Message id : ids) manager.addListener(telegraph, id.ordinal());
    }

    public static void removeListener(Telegraph telegraph, Message... ids) {
        for(Message id : ids) manager.removeListener(telegraph, id.ordinal());
    }
}
