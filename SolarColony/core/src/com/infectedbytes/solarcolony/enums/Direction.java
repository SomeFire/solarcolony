package com.infectedbytes.solarcolony.enums;

/**
 * Created by Henrik on 22.12.2015.
 */
public enum Direction {
    NORTH, EAST, SOUTH, WEST;

    public static Direction valueOf(int ordinal) { return values()[ordinal]; }

    public static Direction valueOf(float dx, float dy) {
        float ax = Math.abs(dx);
        float ay = Math.abs(dy);
        if(ax > ay) return dx > 0 ? EAST : WEST;
        return dy > 0 ? NORTH : SOUTH;
    }
}
