package com.infectedbytes.solarcolony;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.infectedbytes.solarcolony.enums.Direction;

/**
 * Created by Henrik on 22.12.2015.
 */
public final class Resources {
    public static Texture roomSheet;
    public static Texture characterSheetMale;
    public static Texture characterSheetFemale;
    public static TextureRegion connectorSheet;
    private static TextureRegion[] connectorDirections = new TextureRegion[4];
    public static CharacterSheet[] characterSheetsMale;
    public static CharacterSheet[] characterSheetsFemale;
    public static Skin skin;
    public static Names names;
    public static Texture icons;
    public static TextureRegion exclamationMark;
    public static TextureRegion hourglass;
    public static TextureRegion offline;
    public static TextureRegion health;
    public static TextureRegion oxygen;
    public static TextureRegion hunger;
    public static TextureRegion thirst;
    public static TextureRegion delete;
    public static Texture background;
    public static Texture asteroids;
    private static Array<Asteroid> asteroidArray;
    private static int asteroidProbability;

    public static String randomName(boolean female) {
        String[] arr = female ? names.female : names.male;
        return arr[Util.nextInt(arr.length)];
    }

    private Resources() { }

    @SuppressWarnings("unchecked")
    public static void load() {
        int thickness = Util.CONNECTOR_THICKNESS;
        int width = Util.CONNECTOR_WIDTH;
        background = new Texture("galaxy.png");
        asteroids = new Texture("asteroids.png");
        roomSheet = new Texture("rooms.png");
        connectorSheet = new TextureRegion(roomSheet, 0, 0, Util.TILE_SIZE, Util.TILE_SIZE);
        connectorDirections[Direction.NORTH.ordinal()] = new TextureRegion(connectorSheet, thickness, 0, width, thickness);
        connectorDirections[Direction.SOUTH.ordinal()] = new TextureRegion(connectorSheet, thickness, width + thickness, width, thickness);
        connectorDirections[Direction.WEST.ordinal()] = new TextureRegion(connectorSheet, 0, thickness, thickness, width);
        connectorDirections[Direction.EAST.ordinal()] = new TextureRegion(connectorSheet, width + thickness, thickness, thickness, width);

        icons = new Texture("icons.png");
        exclamationMark = new TextureRegion(icons, 0, 0, 16, 16);
        hourglass = new TextureRegion(icons, 16, 0, 16, 16);
        offline = new TextureRegion(icons, 32, 0, 16, 16);
        health = new TextureRegion(icons, 0, 16, 16, 16);
        oxygen = new TextureRegion(icons, 16, 16, 16, 16);
        hunger = new TextureRegion(icons, 32, 16, 16, 16);
        thirst = new TextureRegion(icons, 48, 16, 16, 16);
        delete = new TextureRegion(icons, 48, 0, 16, 16);

        characterSheetMale = new Texture("Men.png");
        characterSheetFemale = new Texture("Women.png");
        int n = characterSheetMale.getWidth() / Util.CHARACTER_WIDTH / 3;
        characterSheetsMale = new CharacterSheet[n];
        characterSheetsFemale = new CharacterSheet[n];
        for(int i = 0; i < n; i++) {
            characterSheetsMale[i] = new CharacterSheet(new TextureRegion(characterSheetMale,
                    i * Util.CHARACTER_WIDTH * 3, 0,
                    Util.CHARACTER_WIDTH * 3, Util.CHARACTER_HEIGHT * 4));
            characterSheetsFemale[i] = new CharacterSheet(new TextureRegion(characterSheetFemale,
                    i * Util.CHARACTER_WIDTH * 3, 0,
                    Util.CHARACTER_WIDTH * 3, Util.CHARACTER_HEIGHT * 4));
        }
        skin = new Skin(Gdx.files.internal("uiskin.json"));
        skin.getFont("default-font").getData().markupEnabled = true;

        names = Util.json.fromJson(Names.class, Gdx.files.internal("names.json"));
        asteroidArray = Util.json.fromJson(Array.class, Asteroid.class, Gdx.files.internal("asteroids.json"));
        for(Asteroid a : asteroidArray) asteroidProbability += a.probability;
    }

    public static Asteroid newAsteroid() {
        int rnd = Util.nextInt(asteroidProbability);
        for(Asteroid a : asteroidArray) {
            if(rnd <= a.probability) return new Asteroid(a);
            else rnd -= a.probability;
        }
        return new Asteroid(asteroidArray.get(0));
    }

    public static TextureRegion getDirection(Direction direction) { return connectorDirections[direction.ordinal()]; }

    public static class Names {
        public String[] male;
        public String[] female;
    }
}
